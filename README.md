# SAP CAP MQTT Client

This repository is part of a tutorial explaining the development of an SAP CAP application listening and reacting to MQTT messages. Find out more about the background of this application [here]() (link pending).

## Getting Started

- Install [node.js](https://nodejs.org/).
- Install `@sap/cds-dk` globally using `npm install -g @sap/cds-dk`.
- Configure the application using `/config.js` and the tutorial provided [here]() (link pending).
- Run the application locally using `cds watch`.
